<?php
/*
Template Name: StaticPage
*/
?>
<?php get_header(); ?>
<div class="content-wrapper">
    <div class="container">
        <div class="row-fluid">
            <div class="span12">
                <h2><?php the_title(); ?></h2>
            </div>
        </div>
        <div class="row-fluid">        
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <!--<article class="post" id="post-<?php the_ID(); ?>">-->
                <!-- removed for st. catherines template -->
                <?php /*include (TEMPLATEPATH . '/_/inc/meta.php' );*/ ?>
                <!-- <div class="entry"> -->
                <?php the_content(); ?>
                <!-- </div> -->
                <?php /*edit_post_link('Edit this entry.', '<p>', '</p>');*/ ?>
            <!--</article>-->
            <?php /*comments_template();*/ ?>
        <?php endwhile; endif; ?>        
        </div>
    </div>
</div>
<?php get_footer(); ?>