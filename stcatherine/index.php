<?php get_header(); ?>
    <div class="carousel-wrapper">
        <div class="container row carousel-bg">
            <div class="span7">
                <div id="home-carousel" class="carousel slide">
                    <ol class="carousel-indicators">
                        <li data-target="#home-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#home-carousel" data-slide-to="1" class=""></li>
                        <li data-target="#home-carousel" data-slide-to="2" class=""></li>
                    </ol>
                    <!-- Carousel Items -->
                    <div class="carousel-inner">
                        <div class="active item">
                            <?php echo '<img src="' . get_bloginfo('template_url') . '/images/classroom.jpg" />'; ?>
                            <div class="carousel-caption">
                                <h4>Book Club</h4>
                                <p>Children enjoyed reading about a variety of characters and stories this year.</p>
                            </div>
                        </div>
                        <div class="item">
                            <?php echo '<img src="' . get_bloginfo('template_url') . '/images/ipads.jpg" />'; ?>
                            <div class="carousel-caption">
                                <h4>iPads are here!</h4>
                                <p>The school purcahsed a new iPad cart that is shared across all the rooms.</p>
                            </div>
                        </div>
                        <div class="item">
                            <?php echo '<img src="' . get_bloginfo('template_url') . '/images/headphones.jpg" />'; ?>
                            <div class="carousel-caption">
                                <h4>New Story Time</h4>
                                <p>Gather 'round and listen to learning stories that build critical thinking skills.</p>
                            </div>
                        </div>
                    </div>
                    <!-- Carousel Nav -->
                    <a class="carousel-control left" href="#home-carousel" data-slide="prev">&lsaquo;</a>
                    <a class="carousel-control right" href="#home-carousel" data-slide="next">&rsaquo;</a>
                </div>
            </div>
            <div class="span5 carousel-static">
                <h4>Announcements</h4>
                <ul class="announcements unstyled">
<?php   $announcementArgs = array( 'category_name' => 'Announcements', 'numberposts' => 6 );
        $announcements = get_posts($announcementArgs);
        foreach($announcements as $a) {
            echo '<li><a href="'.$a->post_name.'">'.$a->post_title.'</a><span class="li-news-date pull-right">'.date("M d", strtotime($a->post_date)).'</span></li>';
        } ?>
                </ul>
                <a href="<?php echo get_page_link(29); ?>">More...</a>
            </div>
        </div>
    </div>
    
    <div class="content-wrapper">
        <div class="container row">
            <div class="span8">
<?php   $args = array( 'category_name' => 'Spotlight', 'numberposts' => 1 );
        $posts = get_posts($args);
        foreach($posts as $p) {
            echo '<h3><img src="' . get_bloginfo('template_url') . '/images/sc-sports-logo.jpg" height="8%" width="8%" />';
            echo $p->post_title.'</h3><div>'.$p->post_content.'</div>';
        } ?>
            </div>
            <div class="span4 pull-right">
                <h3>This Week</h3>
                <div class=" well">
                    <ul>
<?php   $eventArgs = array( 'category' => 12 );
        $events = get_posts($eventArgs);
        foreach($events as $e) {
            echo '<li><a href="'.$e->post_name.'">'.$e->post_title.'</a></li>';
        } ?>
                    </ul>
                    <a href="/sca/our-school/calendar">See the Full Calendar</a>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
