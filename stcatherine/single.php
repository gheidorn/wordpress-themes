<?php get_header(); ?>
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="span12">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
                
                <h2 class="entry-title"><?php the_title(); ?></h2>
                <em><?php include (TEMPLATEPATH . '/_/inc/meta.php' ); ?></em>
                
                <div class="entry-content">
                    
                    <?php the_content(); ?>

                    <?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
                    
                    <?php the_tags( 'Tags: ', ', ', ''); ?>
                
                    

                </div>
                
                <?php /*edit_post_link('Edit this entry','','.');*/ ?>
                
            </article>

        <?php //comments_template(); ?>

        <?php endwhile; endif; ?>
        
            </div>
        </div>
</div>
</div>
<?php //get_sidebar(); ?>

<?php get_footer(); ?>