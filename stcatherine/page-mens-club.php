<?php
/*
Template Name: MensClub
*/
?>
<?php get_header(); ?>
<div class="container">
    <div class="row-fluid">
        <div class="span12">
            <h2><?php the_title(); ?></h2>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span7">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; endif; ?>
        </div>
        <div class="span5">
            <div class="well">
                <h4>Latest News</h4>
                <ul class="unstyled">
                <?php
                $args = array( 'category_name' => 'Men\'s Club', 'numberposts' => 10 );
                $posts = get_posts($args);
                foreach($posts as $p) {
                    echo '<li><a href="'.$p->post_name.'">'.$p->post_title.'</a><span class="pull-right">'.date("M d", strtotime($p->post_date)).'</span></li>';
                } ?>
                </ul>
                <a href="">Archive</a>
            </div>
            <div class="well">
                <h4>2012-2013 Leadership</h4>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>President</th>
                            <td>Phil Griffin<br />philip.griffin@rsmi.com<br />708-634-4900</td>
                        </tr>
                        <tr>
                            <th>Internal Vice President</th>
                            <td>Tom Phelan<br />falo98@aol.com<br />708-425-3512</td>
                        </tr>
                        <tr>
                            <th>External Vice President</th>
                            <td>John Rolence<br />rolly125@aol.com</td>
                        </tr>
                        <tr>
                            <th>Treasurer</th>
                            <td>Chris Ivers<br />ivers4235@sbcglobal.net<br />708-424-4186</td>
                        </tr>
                        <tr>
                            <th>Social Chairman</th>
                            <td>Sean Kelly<br />SeanFKelly@comcast.net<br />708-424-6476</td>
                        </tr>
                        <tr>
                            <th>Secretary</th>
                            <td>Jim Burke<br />jburke_11@yahoo.com</td>
                        <tr>
                            <th>Sergeant at Arms</th>
                            <td>Ed Arquilla<br />ed.arquilla@ups-scs.com</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>