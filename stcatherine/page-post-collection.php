<?php
/*
Template Name: CollectionOfPosts
*/
?>
<?php get_header(); ?>
<div class="container">
    <div class="row-fluid">
        <div class="span12">
            <h2><?php the_title(); ?></h2>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="accordion" id="accordion2">
<?php   $posts_array = get_posts( array('category_name' => 'Announcements', 'numberposts' => 5) );
        foreach($posts_array as $post) { ?>
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $post->ID;?>">
                            <?php echo $post->post_title; ?>
                            
                            <span class="pull-right"><?php echo date("M d Y", strtotime($post->post_date)) ?></span>
                            <span class="pull-right">&nbsp;&nbsp;</span>
<?php                       $category = get_the_category($post->ID);
                    
                            echo '<span class="pull-right"><i>' . $category[0]->cat_name .'</i></span>';
?>
                            
                        </a>
                        
                    </div>
                    <div id="collapse<?php echo $post->ID;?>" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <?php echo $post->post_content; ?>
                        </div>
                    </div>
                </div>
<?php   } ?> 
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>