<!DOCTYPE html>

<!--[if lt IE 7 ]> <html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head id="www-sitename-com" data-template-set="html5-reset-wordpress-theme" profile="http://gmpg.org/xfn/11">
<link href='http://fonts.googleapis.com/css?family=Metamorphous' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <meta charset="<?php bloginfo('charset'); ?>">
    
    <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    
    <?php if (is_search()) { ?>
    <meta name="robots" content="noindex, nofollow" /> 
    <?php } ?>

    <title>
           <?php
              if (function_exists('is_tag') && is_tag()) {
                 single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
              elseif (is_archive()) {
                 wp_title(''); echo ' Archive - '; }
              elseif (is_search()) {
                 echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
              elseif (!(is_404()) && (is_single()) || (is_page())) {
                 wp_title(''); echo ' - '; }
              elseif (is_404()) {
                 echo 'Not Found - '; }
              if (is_home()) {
                 bloginfo('name'); echo ' - '; bloginfo('description'); }
              else {
                  bloginfo('name'); }
              if ($paged>1) {
                 echo ' - page '. $paged; }
           ?>
    </title>
    
    <meta name="title" content="<?php
              if (function_exists('is_tag') && is_tag()) {
                 single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
              elseif (is_archive()) {
                 wp_title(''); echo ' Archive - '; }
              elseif (is_search()) {
                 echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
              elseif (!(is_404()) && (is_single()) || (is_page())) {
                 wp_title(''); echo ' - '; }
              elseif (is_404()) {
                 echo 'Not Found - '; }
              if (is_home()) {
                 bloginfo('name'); echo ' - '; bloginfo('description'); }
              else {
                  bloginfo('name'); }
              if ($paged>1) {
                 echo ' - page '. $paged; }
           ?>">
    <meta name="description" content="<?php bloginfo('description'); ?>">
    
    <meta name="google-site-verification" content="">
    <!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->
    
    <meta name="author" content="Your Name Here">
    <meta name="Copyright" content="Copyright Your Name Here 2011. All Rights Reserved.">

    <!-- Dublin Core Metadata : http://dublincore.org/ -->
    <meta name="DC.title" content="Project Name">
    <meta name="DC.subject" content="What you're about.">
    <meta name="DC.creator" content="Who made this site.">
    
    <!--  Mobile Viewport meta tag
    j.mp/mobileviewport & davidbcalhoun.com/2010/viewport-metatag 
    device-width : Occupy full width of the screen in its current orientation
    initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
    maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width -->
    <!-- Uncomment to use; use thoughtfully!
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    -->
    
    <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/_/img/favicon.ico">
    <!-- This is the traditional favicon.
         - size: 16x16 or 32x32
         - transparency is OK
         - see wikipedia for info on browser support: http://mky.be/favicon/ -->
         
    <link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/_/img/apple-touch-icon.png">
    <!-- The is the icon for iOS's Web Clip.
         - size: 57x57 for older iPhones, 72x72 for iPads, 114x114 for iPhone4's retina display (IMHO, just go ahead and use the biggest one)
         - To prevent iOS from applying its styles to the icon name it thusly: apple-touch-icon-precomposed.png
         - Transparency is not recommended (iOS will put a black BG behind the icon) -->
    
    <!-- CSS: screen, mobile & print are all in the same file -->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style-sca.css">

    <!-- all our JS is at the bottom of the page, except for Modernizr. -->
    <script src="<?php bloginfo('template_directory'); ?>/_/js/modernizr-1.7.min.js"></script>
    
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

    <?php wp_head(); ?>
    
</head>

<body <?php body_class(); ?>>

    <!--<div class="container">-->
    <div class="masthead-wrapper">
        <div class="container">
            <div class="span4">
                <a href="<?php echo home_url( '/' ); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/sca-header-logo.gif" /></a>
            </div>
            <div class="motto span4">
                <h3>Reverence</h3>
                <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Respect</h3>
                <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Responsiblity</h3>
            </div>
            <div class="motto span4 pull-right">
                <?php echo '<img src="' . get_bloginfo('template_url') . '/images/sca-bw-logo-circle.gif" height="60%" width="60%" />'; ?>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
        <div class="navbar-wrapper">
            <div class="container">
                <div class="span12">
                    <ul class="nav nav-pills">
                        <?php
function page_sorter($key) {
    return function ($a, $b) use ($key) {
        return ($a[$key] < $b[$key]) ? -1 : 1;
    };
    /*
    if($a['menu_order'] == $b['menu_order']) {
        return 0;
    }
    
    return ($a['menu_order'] < $b['menu_order']) ? -1 : 1;
    
    return 0;
    */
}
                        
                        
                            $args = array('title_li' => '', 'parent' => 0, 'sort_column' => 'menu_order');
                            $pages = get_pages( $args );
                            foreach ($pages as $page) {
                                echo '<li class="dropdown">';
                                echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$page->post_title.' <b class="caret"></b></a>';
                                
                                $subPages = get_children($page->ID);
                                
                                if(!empty($subPages)) {
                                    echo '<ul class="dropdown-menu">';
                                    
                                    //usort($subPages, page_sorter("menu_order"));

                                    foreach ($subPages as $subPage) {
                                        $subSubPages = get_children($subPage->ID);
                                        // check for children of children (for flyout submenu)
                                        if(!empty($subSubPages)) {
                                            echo '<li class="dropdown-submenu"><a tabindex="-1" href="'.get_page_link($subPage->ID).'">'.$subPage->post_title.'</a><ul class="dropdown-menu">';
                                            foreach ($subSubPages as $subSubPage) {
                                                echo '<li><a href="'.get_page_link($subSubPage->ID).'">'.$subSubPage->post_title.'</a></li>';
                                            }
                                            echo '</ul></li>';
                                        } else {
                                            echo '<li><a href="'.get_page_link($subPage->ID).'">'.$subPage->post_title.'</a></li>';
                                        }
                                    }
                                    echo '</ul>';
                                }
                                echo '</li>';
                            }
                        ?> 
                    </ul>
                </div>
            </div>
        </div>