
        <hr/
        >  
    
  <!--</div>--><!-- // container -->

<div class="footer-wrapper">
    <div class="container">
        <div class="row-fluid footer-inner">
            <div class="span3">
                <?php wp_nav_menu( array('menu' => 'Welcome Menu' )); ?>
            </div>
            <div class="span3">
                <?php wp_nav_menu( array('menu' => 'Teaching & Learning Menu' )); ?>
            </div>
            <div class="span3">
                <?php wp_nav_menu( array('menu' => 'Parent Organization Menu' )); ?>
            </div>
            <div class="span3">
                <?php wp_nav_menu( array('menu' => 'Our Parish Menu' )); ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span4">
                <p>&copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?></p>
                <img src="https://twitter.com/images/resources/twitter-bird-blue-on-white.png" width="30" height="30" />
                <img src="<?php bloginfo('template_directory'); ?>/images/FB-f-Logo__blue_72.png" width="30" height="30" />
            </div>
            <div class="span6 pull-right">
                <p>10621 S. Kedvale Oak Lawn, Illinois 60453<br/>
                School Phone 708.425.5547<br />School Fax 708.425.3701<br />Rectory Phone 708.425.2850<br />Rectory Fax 708.425.2313</p>
            </div>
            <div style="clear:both;"></div>
        </div>
      <!-- </footer> -->
    </div>
  </div>  

  <?php wp_footer(); ?>


<!-- here comes the javascript -->

<!-- jQuery is called via the Wordpress-friendly way via functions.php -->

<!-- this is where we put our custom functions -->
<script src="<?php bloginfo('template_directory'); ?>/_/js/functions.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/bootstrap/js/bootstrap.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/bootstrap/js/bootstrap-dropdown.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/bootstrap/js/bootstrap-carousel.js"></script>
<script type="text/javascript">  
        $(document).ready(function () {  
            $('.dropdown-toggle').dropdown();
            $('#home-carousel').carousel();
        });  
   </script>  
<!-- Asynchronous google analytics; this is the official snippet.
     Replace UA-XXXXXX-XX with your site's ID and uncomment to enable.
     
<script>

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-XXXXXX-XX']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
-->
    
</body>

</html>
