<?php
/*
Template Name: SchoolBoard
*/
?>
<?php get_header(); ?>
<div class="container">
    <div class="row-fluid">
        <div class="span12">
            <h2><?php the_title(); ?></h2>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span7">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; endif; ?>
        </div>
        <div class="span5">
            <div class="well">
                <h4>Latest News</h4>
                <ul class="unstyled">
                <?php
                $args = array( 'category_name' => 'School Board', 'numberposts' => 10 );
                $posts = get_posts($args);
                foreach($posts as $p) {
                    echo '<li><a href="'.$p->post_name.'">'.$p->post_title.'</a><span class="pull-right">'.date("M d", strtotime($p->post_date)).'</span></li>';
                } ?>
                </ul>
                <a href="">Archive</a>
            </div>
            <div class="well">
                <h4>2012-2013 Board</h4>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>President</th>
                            <td>Bill Veal</td>
                        </tr>
                        <tr>
                            <th>Vice President</th>
                            <td>Donna Marie Ivers</td>
                        </tr>
                        <tr>
                            <th>Secretary</th>
                            <td>Andrea Kavanaugh</td>
                        <tr>
                            <th>Members</th>
                            <td>Jeanine Cunnea<br />Katie Hanley<br />Dan Kelleher<br />Tony Martin<br />Brian Nolan<br />Kelly Withers</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>