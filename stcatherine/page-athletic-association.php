<?php
/*
Template Name: AthleticAssociation
*/
?>
<?php get_header(); ?>
<div class="container">
    <div class="row-fluid">
        <div class="span12">
            <h2><?php the_title(); ?></h2>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span7">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; endif; ?>
        </div>
        <div class="span5">
            <div class="well">
                <h4>Latest News</h4>
                <ul class="unstyled">
                <?php
                $args = array( 'category_name' => 'Athletic Association', 'numberposts' => 10 );
                $posts = get_posts($args);
                foreach($posts as $p) {
                    echo '<li><a href="'.$p->post_name.'">'.$p->post_title.'</a><span class="pull-right">'.date("M d", strtotime($p->post_date)).'</span></li>';
                } ?>
                </ul>
                <a href="">Archive</a>
            </div>
            <div class="well">
                <h4>2012-2013 Leadership</h4>
                <table class="table">
                <tr>
                <th>President</th><td>Ron Badali</td>
                </tr>
                <tr>
                <th>Treasurer</th>
                <td>Kevin Hughes</td>
                </tr>
                <tr>
                <th>Vice President</th>
                <td>Scott McCabe</td>
                </tr>
                <tr>
                <th>Secretary</th>
                <td>Reenie Panella</td>
                </tr>
                <tr>
                <th>Volunteer Coordinator</th>
                <td>Ginny Buschman</td>
                </tr>
                </table>
                <h4>Sport Chairpersons</h4>
                <table class="table">
                <tr>
                <th>Football</th>
                <td>Tom Maher</td>
                </tr>
                <tr>
                <th>Cheerleading</th>
                <td>Reenie Panella</td>
                </tr>
                <tr>
                <th>Boys Basketball</th>
                <td>Ed Arquilla, Scott McCabe, John Roche</td>
                </tr>
                <tr>
                <th>Girls Basketball</th>
                <td>John Campbell, John Kelley</td>
                </tr>
                <tr>
                <th>Boys Volleyball</th>
                <td>Tim Gainer</td>
                </tr>
                <tr>
                <th>Girls Volleyball</th>
                <td>Maureen Brennan</td>
                </tr>
                <tr>
                <th>Soccer</th>
                <td>Mike Musto, Mark Wozniczka</td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>